//Taak1 CP1 
//Benabdelali Nabil
// 1BaMCT A
//nabil.benabdelali@student.ehb.be

//import processing.pdf.*;
//PDF,"Taak1_Benabdelali-Nabil.pdf"
void setup() {
  background(0);
  size(600, 800);
  drawpoint();
  drawEllipse();
}
void drawpoint(){

  for(int i = 0; i<500; i++){ // Hier maak ik een variable i aan die gelijk is aan 0, I is dan kleiner dan mijn int i dus 500 en i++ telt altijd 1 er bij.
    noFill(); // Geen kleur voor mijn tekeningen behalve mijn randen.
    stroke(random(100), random(0, 100), random(0, 100), 100);  // Kleur van mijn randen.
   strokeWeight(0.9);  // Dikte van mijn strokke (lijnen).
    rect(random(width+i),random(height+i),50,50); // Random position van mijn vierkanten en de size van mijn vierkanten ( hoogte en breedte ).
  }
}

void drawEllipse() {
  int circle = 100; // Hier maak ik een variable circle aan voor het in meerdere methodes te kunnen gebruiken.
  translate(width/2, height/2); // Ik verplaats mijn begin punt (0,0) naar het midden van mijn width/2 & height/2.
  noFill();  // Geen kleur voor mijn tekeningen ( behalve bij mijn randen ).
  strokeWeight(0.9);  // dikte van mijn stroke
  for (int Y = 0; Y < circle; Y++) { // Ik maak een variable Y aan die gelijk is aan 0, Y is dan kleiner dan circle dus 100 en Y++ telt altijd 1 er bij.
    stroke(random(100), random(0, 100), random(0, 100), 100);  // Kleur van mijn randen.
    ellipse(Y+1, 10, 320, 280); // Y+1 zo dat mijn lijnen dichter bij de andere staan, 10 tweede positie van min ellipse 320 en 280 hoogte en breedte.
    rotate(TWO_PI / width * height + 100);  // Roteren van meerdere ellipse 6,28 delen door mijn width maal mijn height + 100.
  }
}